package com.example;

public class Main {
    public static void main(String[] args) {

        StudentA studentA = new StudentA(80, 70, 60);
        System.out.println("Percentage of marks for student A: " + studentA.getPercentage());


        StudentB studentB = new StudentB(80, 70, 60, 50);
        System.out.println("Percentage of marks for student B: " + studentB.getPercentage());
    }
}
